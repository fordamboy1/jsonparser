from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / 'README.txt').read_text(encoding='utf-8')

setup(
	name = 'jdfoobar',
	version = '1.0.0',
	description = 'Sum foo or bar numbers',
	long_description = long_description,
	#long_description_content_type='text/plain',
	author = 'Jacob DeRosa',
	packages = ['pckg'],
	python_requires = '>=3',
	#package_data = { 'pckg': ['example.json']},
	#entry_points = {'console_scripts': ['foobar=pckg:main']}
)