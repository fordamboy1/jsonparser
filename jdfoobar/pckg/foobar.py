# Jacob DeRosa

# parses input JSON file and sums either 'foo' or 'bar' numbers

import sys
import argparse
import json

def jsonparse(args):
	parser = argparse.ArgumentParser(description="Sum up 'foo' or 'bar' numbers.")
	parser.add_argument("json_file", help="json file")
	parser.add_argument("foo_bar", help = "select foo or bar to sum")
	parser.add_argument("--debug", help = "add --debug to view the inputed JSON file", action = "store_true")
	args = parser.parse_args(args)
	if(args.foo_bar != 'foo' and args.foo_bar != 'bar'):
		raise ValueError("Invalid selection, use -h to see usage")
	with open(args.json_file) as f:
		data = json.load(f)
	if(args.debug):
		print(data)
	sum = 0
	for dic in data:
		#if(args.foo_bar in dic.keys()):
		#	sum += dic[args.foo_bar]
		sum += dic.get(args.foo_bar, 0)
	print("SUM for {} numbers: {}".format(args.foo_bar, sum))