FROM ubuntu

RUN apt-get update

RUN apt-get install python3.6

RUN apt-get install -y python3-pip

COPY jdfoobar /jdfoobar

RUN pip3 install /jdfoobar

COPY example.json /example.json

#command line to execute python script, last argument can be changed to add foo numbers instead of bar
CMD ["python3", "-m", "pckg", "/example.json", "bar"]